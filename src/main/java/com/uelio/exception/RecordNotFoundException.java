package com.uelio.exception;

public class RecordNotFoundException extends RuntimeException{

    private static final long serialVersionUID = 1L;

    public RecordNotFoundException(Long id){

        super("Register Not Found with id: " + id);

    }
    
}
