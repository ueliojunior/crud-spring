package com.uelio;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import com.uelio.enums.Category;
import com.uelio.model.Course;
import com.uelio.model.Lesson;
import com.uelio.repository.CourseRepository;

@SpringBootApplication
public class CrudSpringApplication {

	public static void main(String[] args) {
		SpringApplication.run(CrudSpringApplication.class, args);
	}

	@Bean
	CommandLineRunner initDatabase(CourseRepository courseRepository) {

		return args -> {
			courseRepository.deleteAll();

			Course c = new Course();

			c.setName("Angular com Spring");
			c.setCategory(Category.BACK_END);

			Lesson l = new Lesson();
			l.setName("Introdução");
			l.setYoutubeUrl("https:");
			l.setCourse(c);
			c.getLessons().add(l);

			Lesson a = new Lesson();
			a.setName("Angular");
			a.setYoutubeUrl("https:2");
			a.setCourse(c);
			c.getLessons().add(a);

			courseRepository.save(c);

		};

	}

}
