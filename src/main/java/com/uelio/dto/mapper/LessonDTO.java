package com.uelio.dto.mapper;

public record LessonDTO(

    Long id,
    String name,
    String youtuString) {
    
}
